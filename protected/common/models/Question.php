<?php
namespace common\models;
use Yii;
use \common\models\base\Question as BaseQuestion;

/**
* This is the model class for table "questions".
*/
class Question extends BaseQuestion {

	/**
	* @var array virtual attribute for keeping emails
	*/
	public $answers;

	/**
	* @inheritdoc
	*/
	public static function primaryKey() {
		return ['id'];
	}
	public function fields() {
		return array_merge(['lifelines' =>
		function ($model) {
			return $model->quiz->quizHasLifelines;
		}
		,], parent::fields());
	}
	public function extraFields() {
		return ['answers'];
	}
}