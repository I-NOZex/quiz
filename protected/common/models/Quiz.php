<?php

namespace common\models;

use Yii;
use \common\models\base\Quiz as BaseQuiz;

/**
 * This is the model class for table "quiz".
 */
class Quiz extends BaseQuiz{
	public function extraFields(){
        return ['questions', 'lifelines', 'questions.answers'];
    }
}
