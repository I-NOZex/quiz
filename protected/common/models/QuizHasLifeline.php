<?php

namespace common\models;

use Yii;
use \common\models\base\QuizHasLifeline as BaseQuizHasLifeline;

/**
 * This is the model class for table "quiz_has_lifelines".
 */
class QuizHasLifeline extends BaseQuizHasLifeline
{
}
