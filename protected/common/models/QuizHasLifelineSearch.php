<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\QuizHasLifeline;

/**
* QuizHasLifelineSearch represents the model behind the search form about `common\models\QuizHasLifeline`.
*/
class QuizHasLifelineSearch extends QuizHasLifeline
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['quiz_id', 'lifelines_id', 'used'], 'integer'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = QuizHasLifeline::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'quiz_id' => $this->quiz_id,
            'lifelines_id' => $this->lifelines_id,
            'used' => $this->used,
        ]);

return $dataProvider;
}
}