<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "quiz_has_lifelines".
 *
 * @property integer $quiz_id
 * @property integer $lifelines_id
 * @property integer $used
 *
 * @property \common\models\Lifeline $lifelines
 * @property \common\models\Quiz $quiz
 * @property string $aliasModel
 */
abstract class QuizHasLifeline extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_has_lifelines';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quiz_id', 'lifelines_id'], 'required'],
            [['quiz_id', 'lifelines_id', 'used'], 'integer'],
            [['lifelines_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lifeline::className(), 'targetAttribute' => ['lifelines_id' => 'id']],
            [['quiz_id'], 'exist', 'skipOnError' => true, 'targetClass' => Quiz::className(), 'targetAttribute' => ['quiz_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'quiz_id' => Yii::t('app', 'Quiz ID'),
            'lifelines_id' => Yii::t('app', 'Lifelines ID'),
            'used' => Yii::t('app', 'Used'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLifelines()
    {
        return $this->hasOne(\common\models\Lifeline::className(), ['id' => 'lifelines_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuiz()
    {
        return $this->hasOne(\common\models\Quiz::className(), ['id' => 'quiz_id']);
    }




}
