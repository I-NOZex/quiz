<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "quiz".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \common\models\Question[] $questions
 * @property \common\models\QuizHasLifeline[] $quizHasLifelines
 * @property \common\models\Lifeline[] $lifelines
 * @property string $aliasModel
 */
abstract class Quiz extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(\common\models\Question::className(), ['quiz_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuizHasLifelines()
    {
        return $this->hasMany(\common\models\QuizHasLifeline::className(), ['quiz_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLifelines()
    {
        return $this->hasMany(\common\models\Lifeline::className(), ['id' => 'lifelines_id'])->viaTable('quiz_has_lifelines', ['quiz_id' => 'id']);
    }




}
