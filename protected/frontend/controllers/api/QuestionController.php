<?php

namespace frontend\controllers\api;

/**
* This is the class for REST controller "QuestionController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class QuestionController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Question';
}
