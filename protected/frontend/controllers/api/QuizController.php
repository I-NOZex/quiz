<?php

namespace frontend\controllers\api;

/**
* This is the class for REST controller "QuizController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class QuizController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Quiz';
}
