<?php

use yii\db\Migration;

class m160622_090101_Answer_access extends Migration
{
    /**
     * @var array controller all actions
     */
    public $permisions = [
        "index" => [
            "name" => "frontend_answer_index",
            "description" => "frontend/answer/index"
        ],
        "view" => [
            "name" => "frontend_answer_view",
            "description" => "frontend/answer/view"
        ],
        "create" => [
            "name" => "frontend_answer_create",
            "description" => "frontend/answer/create"
        ],
        "update" => [
            "name" => "frontend_answer_update",
            "description" => "frontend/answer/update"
        ],
        "delete" => [
            "name" => "frontend_answer_delete",
            "description" => "frontend/answer/delete"
        ]
    ];
    
    /**
     * @var array roles and maping to actions/permisions
     */
    public $roles = [
        "FrontendAnswerFull" => [
            "index",
            "view",
            "create",
            "update",
            "delete"
        ],
        "FrontendAnswerView" => [
            "index",
            "view"
        ],
        "FrontendAnswerEdit" => [
            "update",
            "create",
            "delete"
        ]
    ];
    
    public function up()
    {
        
        $permisions = [];
        $auth = \Yii::$app->authManager;

        /**
         * create permisions for each controller action
         */
        foreach ($this->permisions as $action => $permission) {
            $permisions[$action] = $auth->createPermission($permission['name']);
            $permisions[$action]->description = $permission['description'];
            $auth->add($permisions[$action]);
        }

        /**
         *  create roles
         */
        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->add($role);

            /**
             *  to role assign permissions
             */
            foreach ($actions as $action) {
                $auth->addChild($role, $permisions[$action]);
            }
        }
    }

    public function down() {
        $auth = Yii::$app->authManager;

        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->remove($role);
        }

        foreach ($this->permisions as $permission) {
            $authItem = $auth->createPermission($permission['name']);
            $auth->remove($authItem);
        }
    }
}
