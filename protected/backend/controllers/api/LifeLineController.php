<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "LifeLineController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LifeLineController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\LifeLine';
}
