<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "QuizHasLifelineController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class QuizHasLifelineController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\QuizHasLifeline';
}
