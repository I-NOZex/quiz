<?php
namespace backend\controllers;
use \common\models\Answer;

/**
* This is the class for controller "QuestionController".
*/
class QuestionController extends \backend\controllers\base\QuestionController {

	/**
	* Creates a new Question model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	* @return mixed
	*/
	public function actionCreate() {
		$model = new \common\models\Question;
		$modelAns = [new Answer,new Answer,new Answer,new Answer];
		try {
			if ($model->load($_POST) && $model->save()) {
			    $modelAns->questions_id = $model->id;
		        if ($modelAns->load($_POST) && $modelAns->save()) {
				    return $this->redirect(['view', 'id' => $model->id]);
                }
			} elseif (!\Yii::$app->request->isPost) {
				$model->load($_GET);
			}
		}
		catch (\Exception $e) {
			$msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
			$model->addError('_exception', $msg);
		}
		return $this->render('create', [
            'model' => $model,
            'modelAns'=>(empty($modelAns)) ? [new Answer,new Answer,new Answer,new Answer] : $modelAns
        ]);
	}
}