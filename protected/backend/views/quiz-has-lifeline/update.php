<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\QuizHasLifeline $model
*/

$this->title = Yii::t('app', 'QuizHasLifeline') . $model->quiz_id . ', ' . Yii::t('app', 'Edit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'QuizHasLifelines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->quiz_id, 'url' => ['view', 'quiz_id' => $model->quiz_id, 'lifelines_id' => $model->lifelines_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit');
?>
<div class="giiant-crud quiz-has-lifeline-update">

    <h1>
        <?= Yii::t('app', 'QuizHasLifeline') ?>
        <small>
                        <?= $model->quiz_id ?>        </small>
    </h1>

    <div class="crud-navigation">
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'), ['view', 'quiz_id' => $model->quiz_id, 'lifelines_id' => $model->lifelines_id], ['class' => 'btn btn-default']) ?>
    </div>

    <hr />

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
