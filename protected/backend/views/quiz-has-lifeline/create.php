<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\QuizHasLifeline $model
*/

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'QuizHasLifelines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="giiant-crud quiz-has-lifeline-create">

    <h1>
        <?= Yii::t('app', 'QuizHasLifeline') ?>        <small>
                        <?= $model->quiz_id ?>        </small>
    </h1>

    <div class="clearfix crud-navigation">
        <div class="pull-left">
            <?=             Html::a(
            Yii::t('app', 'Cancel'),
            \yii\helpers\Url::previous(),
            ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <hr />

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
