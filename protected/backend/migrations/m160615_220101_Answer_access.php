<?php

use yii\db\Migration;

class m160615_220101_Answer_access extends Migration
{
    /**
     * @var array controller all actions
     */
    public $permisions = [
        "index" => [
            "name" => "backend_answer_index",
            "description" => "backend/answer/index"
        ],
        "view" => [
            "name" => "backend_answer_view",
            "description" => "backend/answer/view"
        ],
        "create" => [
            "name" => "backend_answer_create",
            "description" => "backend/answer/create"
        ],
        "update" => [
            "name" => "backend_answer_update",
            "description" => "backend/answer/update"
        ],
        "delete" => [
            "name" => "backend_answer_delete",
            "description" => "backend/answer/delete"
        ]
    ];
    
    /**
     * @var array roles and maping to actions/permisions
     */
    public $roles = [
        "BackendAnswerFull" => [
            "index",
            "view",
            "create",
            "update",
            "delete"
        ],
        "BackendAnswerView" => [
            "index",
            "view"
        ],
        "BackendAnswerEdit" => [
            "update",
            "create",
            "delete"
        ]
    ];
    
    public function up()
    {
        
        $permisions = [];
        $auth = \Yii::$app->authManager;

        /**
         * create permisions for each controller action
         */
        foreach ($this->permisions as $action => $permission) {
            $permisions[$action] = $auth->createPermission($permission['name']);
            $permisions[$action]->description = $permission['description'];
            $auth->add($permisions[$action]);
        }

        /**
         *  create roles
         */
        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->add($role);

            /**
             *  to role assign permissions
             */
            foreach ($actions as $action) {
                $auth->addChild($role, $permisions[$action]);
            }
        }
    }

    public function down() {
        $auth = Yii::$app->authManager;

        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->remove($role);
        }

        foreach ($this->permisions as $permission) {
            $authItem = $auth->createPermission($permission['name']);
            $auth->remove($authItem);
        }
    }
}
