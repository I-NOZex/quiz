<?php

use yii\db\Migration;

class m160615_220101_Quiz_access extends Migration
{
    /**
     * @var array controller all actions
     */
    public $permisions = [
        "index" => [
            "name" => "backend_quiz_index",
            "description" => "backend/quiz/index"
        ],
        "view" => [
            "name" => "backend_quiz_view",
            "description" => "backend/quiz/view"
        ],
        "create" => [
            "name" => "backend_quiz_create",
            "description" => "backend/quiz/create"
        ],
        "update" => [
            "name" => "backend_quiz_update",
            "description" => "backend/quiz/update"
        ],
        "delete" => [
            "name" => "backend_quiz_delete",
            "description" => "backend/quiz/delete"
        ]
    ];
    
    /**
     * @var array roles and maping to actions/permisions
     */
    public $roles = [
        "BackendQuizFull" => [
            "index",
            "view",
            "create",
            "update",
            "delete"
        ],
        "BackendQuizView" => [
            "index",
            "view"
        ],
        "BackendQuizEdit" => [
            "update",
            "create",
            "delete"
        ]
    ];
    
    public function up()
    {
        
        $permisions = [];
        $auth = \Yii::$app->authManager;

        /**
         * create permisions for each controller action
         */
        foreach ($this->permisions as $action => $permission) {
            $permisions[$action] = $auth->createPermission($permission['name']);
            $permisions[$action]->description = $permission['description'];
            $auth->add($permisions[$action]);
        }

        /**
         *  create roles
         */
        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->add($role);

            /**
             *  to role assign permissions
             */
            foreach ($actions as $action) {
                $auth->addChild($role, $permisions[$action]);
            }
        }
    }

    public function down() {
        $auth = Yii::$app->authManager;

        foreach ($this->roles as $roleName => $actions) {
            $role = $auth->createRole($roleName);
            $auth->remove($role);
        }

        foreach ($this->permisions as $permission) {
            $authItem = $auth->createPermission($permission['name']);
            $auth->remove($authItem);
        }
    }
}
