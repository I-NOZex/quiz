<?php

namespace api\modules\v1\controllers;

/**
* This is the class for REST controller "QuestionController".
*/

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use \common\models\Question;

class QuestionController extends \frontend\controllers\api\QuestionController{

	public $modelClass = '\common\models\Question';
	
	public function behaviors(){
		return [
			[
				'class' => 'yii\filters\HttpCache',
				'etagSeed' => function ($action, $params) {
					\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
					return \common\models\Answer::find()->count();
				},
			],
		];
	}	
	
    /*
    *  allow override of the REST actions here -
    * 
    * 	index	: list of models
    * 	view 	: return the details of a model
    * 	create	: create a new model
    * 	update	: update an existing model
    *	delete	: delete an existing model
    *	options: return the allowed HTTP methods
    *
    * To remove support for any of these actions use unset($actions['action_name_here'], ...)
    */

	public function actions() {
		$actions = parent::actions();

		// disable the "delete" and "create" actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}	

	// if you are doing non-verified stuff must have this set to false
	// so yii doesn't look for the token.
	

	public function actionHello()
	{

		        return new ActiveDataProvider([
            'query' => Question::find(),
        ]);
	}
}
