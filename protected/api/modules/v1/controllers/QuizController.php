<?php

namespace api\modules\v1\controllers;

/**
* This is the class for REST controller "QuizController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class QuizController extends \frontend\controllers\api\QuizController {
	public $modelClass = 'common\models\Quiz';
	
	public function actions() {
		$actions = parent::actions();

		// disable the "delete" and "create" actions
		unset($actions['delete'], $actions['create'], $actions['update']);

		return $actions;
	}	
}
