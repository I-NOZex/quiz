<?php

$vendorDir = dirname(__DIR__);

return array (
  'nenad/yii2-widgets-base' => 
  array (
    'name' => 'nenad/yii2-widgets-base',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@nenad' => $vendorDir . '/nenad/yii2-widgets-base',
    ),
  ),
  'nenad/yii2-password-strength' => 
  array (
    'name' => 'nenad/yii2-password-strength',
    'version' => '2.3.0.0',
    'alias' => 
    array (
      '@nenad/passwordStrength' => $vendorDir . '/nenad/yii2-password-strength',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'mihaildev/yii2-ckeditor' => 
  array (
    'name' => 'mihaildev/yii2-ckeditor',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@mihaildev/ckeditor' => $vendorDir . '/mihaildev/yii2-ckeditor',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'dmstr/yii2-db' => 
  array (
    'name' => 'dmstr/yii2-db',
    'version' => '0.5.1.0',
    'alias' => 
    array (
      '@dmstr/db' => $vendorDir . '/dmstr/yii2-db/db',
      '@dmstr/db/tests' => $vendorDir . '/dmstr/yii2-db/db/tests',
      '@dmstr/console' => $vendorDir . '/dmstr/yii2-db/console',
    ),
  ),
  'dmstr/yii2-helpers' => 
  array (
    'name' => 'dmstr/yii2-helpers',
    'version' => '0.4.0.0',
    'alias' => 
    array (
      '@dmstr/helpers' => $vendorDir . '/dmstr/yii2-helpers/src',
    ),
  ),
  'dmstr/yii2-bootstrap' => 
  array (
    'name' => 'dmstr/yii2-bootstrap',
    'version' => '0.2.0.0',
    'alias' => 
    array (
      '@dmstr/bootstrap' => $vendorDir . '/dmstr/yii2-bootstrap',
    ),
  ),
  'schmunk42/yii2-giiant' => 
  array (
    'name' => 'schmunk42/yii2-giiant',
    'version' => '0.8.3.0',
    'alias' => 
    array (
      '@schmunk42/giiant' => $vendorDir . '/schmunk42/yii2-giiant/src',
    ),
    'bootstrap' => 'schmunk42\\giiant\\Bootstrap',
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.5.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.1.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.7.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-builder' => 
  array (
    'name' => 'kartik-v/yii2-builder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/builder' => $vendorDir . '/kartik-v/yii2-builder',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform',
    ),
  ),
  'unclead/yii2-multiple-input' => 
  array (
    'name' => 'unclead/yii2-multiple-input',
    'version' => '1.2.18.0',
    'alias' => 
    array (
      '@unclead/widgets/examples' => $vendorDir . '/unclead/yii2-multiple-input/examples',
      '@unclead/widgets' => $vendorDir . '/unclead/yii2-multiple-input/src',
    ),
  ),
  'bajadev/yii2-dynamicform' => 
  array (
    'name' => 'bajadev/yii2-dynamicform',
    'version' => '2.4.0.0',
    'alias' => 
    array (
      '@bajadev/dynamicform' => $vendorDir . '/bajadev/yii2-dynamicform/src',
    ),
  ),
);
